/*
 * Copyright (C) 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AURCHECK_AURCHECK_H
#define AURCHECK_AURCHECK_H

#define CACHE_DIR "/var/tmp/aurcheck"

/**
 * @brief Prompt user for Y/n
 *
 * @return int - 1 if positive response, otherwise 0
 */
int conf_prompt();

/**
 * @brief Install package named @p name
 *
 * Downloads package @p name snapshot from AUR.
 *
 * @param name - Package name
 * @param assume_yes - Don't ask questions
 * @param keep_source - Keep build source directory
 * @param compile_only - Build only, don't install
 * @param as_deps - Install as a depedency
 * @param skip_pgp_check - Skip PGP check
 * @return
 */
int install_package(char *name, int assume_yes, int keep_source,
		    int compile_only, int as_deps, int skip_pgp_check);

/**
 * @breif Upgrade all AUR packages
 *
 * Get AUR packages from pacman, fetch their information from AUR, install
 * download and build and install packages upgradable packages.
 *
 * @param upgrade_flag - If 0, only show packages which can be updated
 * @param assume_yes - Don't ask questions
 * @param keep_source - Keep build source directory
 * @param compile_only - Build only, don't install
 * @param as_deps - Install as a depedency
 * @param skip_pgp_check - Skip PGP check
 * @return
 */
int upgrade_packages(int upgrade_flag, int assume_yes, int keep_source,
		     int compile_only, int as_deps, int skip_pgp_check);

#endif //AURCHECK_AURCHECK_H
