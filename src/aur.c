/*
 * Copyright (C) 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "aur.h"
#include "package.h"
#include "aurcheck.h"
#include "version.h"
#include <sys/stat.h>
#include <string.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <json-c/json_tokener.h>
#include <json-c/json_object_iterator.h>

#define AUR_URL "https://aur.archlinux.org"
#define AUR_RPC_URL AUR_URL "/rpc/?v=5&type=info"
#define AUR_VERSION 5

#define AURCHECK_USERAGENT "aurcheck/" VERSION_FULL

struct MemoryStruct {
	char *memory;
	size_t size;
	size_t prev_size;
};

struct Query {
	uint8_t count;
	char **string;
};

static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	return fwrite(ptr, size, nmemb, stream);
}

/* Called by download_info() to allocate the result to memory */
static size_t write_memory_callback(void *contents, size_t size, size_t nmemb,
				    void *user_data)
{
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)user_data;

	mem->memory = realloc(mem->memory, mem->size + realsize + 1);
	if (!mem->memory) {
		fprintf(stderr, "Memory error.");
		exit(1);
	}

	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = '\0';

	return realsize;
}

static void generate_query_string(struct Packages *packages,
				  struct Query *query)
{
	CURL *curl = curl_easy_init();
	uint32_t offset = 0;

	query->count = 1;
	query->string = NULL;

	query->string =
		realloc(query->string, query->count * sizeof(*query->string));
	if (!query->string) {
		fprintf(stderr, "Failed to allocate memory!\n");
		exit(1);
	}

	query->string[0] = malloc(sizeof(char *));
	if (!query->string[0]) {
		fprintf(stderr, "Failed to allocate memory!\n");
		exit(1);
	}

	for (size_t i = 0; i < packages->count; ++i) {
		char *escaped_name =
			curl_easy_escape(curl, packages->arr[i].name,
					 (int)strlen(packages->arr[i].name));
		size_t len = strlen(escaped_name) + 7;

		if (i > 200 || ((offset + len) > 4397)) {
			query->string[query->count - 1][offset + 1] = '\0';
			query->count++;
			offset = 0;
			query->string =
				realloc(query->string,
					query->count * sizeof(*query->string));
			if (!query->string) {
				fprintf(stderr, "Failed to allocate memory!\n");
				exit(1);
			}
			query->string[query->count - 1] = NULL;
		}

		query->string[query->count - 1] = realloc(
			query->string[query->count - 1], (len + offset + 2));
		if (!query->string[query->count - 1]) {
			fprintf(stderr, "Failed to allocate memory!\n");
			exit(1);
		}

		memcpy(&query->string[query->count - 1][offset], "&arg[]=", 7);
		memcpy(&query->string[query->count - 1][offset + 7],
		       escaped_name, strlen(escaped_name));
		query->string[query->count - 1]
			     [offset + 7 + strlen(escaped_name)] = '\0';
		offset += len;

		curl_free(escaped_name);
	}

	curl_easy_cleanup(curl);
}

int cmp_aur_json_int_eq(const json_object *const obj, const char *const key,
			int value)
{
	json_object *tmp;
	int val;

	if (json_object_object_get_ex(obj, key, &tmp) == 0)
		return -10;

	val = json_object_get_int(tmp);
	if (val == value)
		return 0;
	else if (val > value)
		return 1;
	else
		return -1;
}

int cmp_aur_json_string_eq(const json_object *const obj, const char *const key,
			   const char *const value)
{
	json_object *tmp;

	if (json_object_object_get_ex(obj, key, &tmp) == 0)
		return -1;

	return (strcmp(value, json_object_get_string(tmp)) == 0 ? 1 : 0);
}

enum aur_error parse_json(struct Packages *packages, const char *json)
{
	int32_t result_count;
	int32_t parsed_count = 0;

	json_object *obj;
	json_object *obj_result_count;
	json_object *obj_results;

	obj = json_tokener_parse(json);

	if (json_object_object_get_ex(obj, "version", NULL) == 0) {
		fprintf(stderr,
			"AUR version missing from JSON, continuing anyway..\n");
	} else {
		switch (cmp_aur_json_int_eq(obj, "version", AUR_VERSION)) {
		case 1:
			fprintf(stderr,
				"AUR reported newer version than supported, continuing anyway..\n");
			break;
		case -1:
			fprintf(stderr,
				"AUR reported older version than supported, continuing anyway..\n");
			break;
		case 0:
		default:
			break;
		}
	}

	if (json_object_object_get_ex(obj, "type", NULL) == 0)
		return AUR_MISSING_KEY_TYPE;
	else if (cmp_aur_json_string_eq(obj, "type", "error"))
		return AUR_RESPONSE_ERROR;

	if (json_object_object_get_ex(obj, "resultcount", &obj_result_count) ==
	    0)
		return AUR_MISSING_KEY_RESULTCOUNT;
	result_count = json_object_get_int(obj_result_count);
	if (result_count <= 0)
		return AUR_PACKAGE_NOT_FOUND;

	if (json_object_object_get_ex(obj, "results", &obj_results) == 0)
		return AUR_MISSING_KEY_RESULTS;

	for (size_t i = 0; i < json_object_array_length(obj_results); i++) {
		json_object *obj_package;
		json_object *obj_name;
		struct Package *package;
		struct json_object_iterator it;
		struct json_object_iterator itEnd;

		obj_package = json_object_array_get_idx(obj_results, i);
		if (json_object_object_get_ex(obj_package, "Name", &obj_name) ==
		    0)
			continue;

		package = packages_find(packages,
					json_object_get_string(obj_name));
		if (!package)
			continue;
		else
			package->not_found = 0;

		it = json_object_iter_begin(obj_package);
		itEnd = json_object_iter_end(obj_package);

		while (!json_object_iter_equal(&it, &itEnd)) {
			const char *key = json_object_iter_peek_name(&it);

			if (strcmp(key, "PackageBase") == 0) {
				json_object *tmp =
					json_object_iter_peek_value(&it);
				int len = json_object_get_string_len(tmp);

				package->base = calloc(len + 1, sizeof(char));
				if (!package->base) {
					fprintf(stderr, "Memory error.\n");
					exit(1);
				}
				memcpy(package->base,
				       json_object_get_string(tmp), len);
			} else if (strcmp(key, "Version") == 0) {
				json_object *tmp =
					json_object_iter_peek_value(&it);
				int len = json_object_get_string_len(tmp);

				package->version.aur =
					calloc(len + 1, sizeof(char));
				if (!package->version.aur) {
					fprintf(stderr, "Memory error.\n");
					exit(1);
				}

				memcpy(package->version.aur,
				       json_object_get_string(tmp), len);
			} else if (strcmp(key, "URLPath") == 0) {
				json_object *tmp =
					json_object_iter_peek_value(&it);
				int len = json_object_get_string_len(tmp);

				package->url = calloc(len + 1, sizeof(char));
				if (!package->url) {
					fprintf(stderr, "Memory error.\n");
					exit(1);
				}

				memcpy(package->url,
				       json_object_get_string(tmp), len);
			}
			json_object_iter_next(&it);
		}
		parsed_count++;
	}

	return (parsed_count == result_count ? AUR_OK :
					       AUR_PARSING_COUNT_MISMATCH);
}

const char *aur_get_error(enum aur_error error)
{
	char *message;

	switch (error) {
	case AUR_HTTP_ERROR:
		message = "HTTP request returned error.";
		break;
	case AUR_MISSING_KEY_TYPE:
		message = "AUR RPC JSON does not include \"type\" key.";
		break;
	case AUR_MISSING_KEY_RESULTCOUNT:
		message = "AUR RPC does not include \"resultcount\" key.";
		break;
	case AUR_MISSING_KEY_RESULTS:
		message = "AUR RPC does not include \"results\" key.";
		break;
	case AUR_RESPONSE_ERROR:
		message = "AUR RPC returned error.";
		break;
	case AUR_PACKAGE_NOT_FOUND:
		message = "Package not found.";
		break;
	default:
		message = "unknown error.";
		break;
	}

	return message;
}

enum aur_error aur_get_info(struct Packages *packages)
{
	enum aur_error ret = AUR_OK;
	struct Query query;
	CURL *curl_handle;

	generate_query_string(packages, &query);

	curl_global_init(CURL_GLOBAL_ALL);

	for (uint8_t i = 0; i < query.count; ++i) {
		struct MemoryStruct chunk;
		size_t url_len;
		char *url;

		chunk.size = 0;
		chunk.prev_size = 0;
		chunk.memory = malloc(1);
		if (!chunk.memory) {
			fprintf(stderr, "Failed to allocate memory!\n");
			exit(1);
		}

		url_len = strlen(AUR_RPC_URL) + strlen(query.string[i]);
		url = malloc(url_len + 1);
		if (!url) {
			fprintf(stderr, "Failed to allocate memory!\n");
			exit(1);
		}
		snprintf(url, url_len + 1, "%s%s", AUR_RPC_URL,
			 query.string[i]);

		curl_handle = curl_easy_init();
		curl_easy_setopt(curl_handle, CURLOPT_URL, url);
		curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION,
				 write_memory_callback);
		curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA,
				 (void *)&chunk);
		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT,
				 AURCHECK_USERAGENT);

		if (curl_easy_perform(curl_handle) != CURLE_OK) {
			ret = AUR_HTTP_ERROR;
			free(chunk.memory);
			free(url);
			break;
		}

		curl_easy_cleanup(curl_handle);

		ret = parse_json(packages, chunk.memory);
		if (ret != AUR_OK) {
			free(chunk.memory);
			free(url);
			break;
		}

		free(chunk.memory);
		free(url);
	}

	curl_global_cleanup();

	for (uint8_t i = 0; i < query.count; i++)
		free(query.string[i]);
	free(query.string);

	return ret;
}

int download_package(char *name, char *uri)
{
	size_t url_len;
	char *url;
	CURL *curl_handle;
	CURLcode res;
	FILE *fp;
	struct stat st;
	char *package_path;
	size_t len;

	curl_handle = curl_easy_init();
	if (!curl_handle)
		return 1;

	if (stat(CACHE_DIR, &st) == -1) {
		if (mkdir(CACHE_DIR, 0700) != 0) {
			fprintf(stderr,
				"Unable to create temporary directory: %s\n",
				CACHE_DIR);
			exit(1);
		}
	}

	len = snprintf(NULL, 0, "%s/%s.tar.gz", CACHE_DIR, name);
	package_path = malloc(len + 1);
	snprintf(package_path, len + 1, "%s/%s.tar.gz", CACHE_DIR, name);

	fp = fopen(package_path, "wb");
	if (!fp) {
		fprintf(stderr, "Unable to write package file: %s\n",
			package_path);
		return 1;
	}

	url_len = strlen(AUR_URL) + strlen(uri) + 1;
	url = malloc(url_len);
	if (!url) {
		fprintf(stderr, "Memory error.\n");
		exit(1);
	}
	snprintf(url, url_len, "%s%s", AUR_URL, uri);

	curl_easy_setopt(curl_handle, CURLOPT_URL, url);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, fp);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, AURCHECK_USERAGENT);
	curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
	res = curl_easy_perform(curl_handle);
	if (res != CURLE_OK) {
		fclose(fp);
		remove(package_path);
		return 1;
	}

	curl_easy_cleanup(curl_handle);
	fclose(fp);
	free(package_path);

	return 0;
}
