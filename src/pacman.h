/*
 * Copyright (C) 2016-2017, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AURCHECK_PACMAN_H
#define AURCHECK_PACMAN_H

#include "package.h"

/**
 * @brief Get AUR packages from pacman
 *
 * @param packages
 * @return int - Number of AUR packages or -1 on error
 */
int pacman_get_aur_packages(struct Packages *packages);

/**
 * @brief Get '*.pkg.tar.*' packages in current working directory
 *
 * @return char * - String containing all found .pkg.tar. files
 */
char *pacman_get_built_packages();

/**
 * @brief Install package via pacman
 *
 * @param cache_path
 * @param pkg_base
 * @param as_deps
 * @return int - pacman exit code
 */
int pacman_install(char *cache_path, char *pkg_base, int as_deps);

#endif //AURCHECK_PACMAN_H
