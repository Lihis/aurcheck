/*
 * Copyright (C) 2016, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AURCHECK_EXTRACT_H
#define AURCHECK_EXTRACT_H

/**
 * @brief Extract @p filename stored in @p cache_path
 *
 * @param cache_path
 * @param filename
 * @return
 */
int extract_package(char *cache_path, char *filename);

#endif //AURCHECK_EXTRACT_H
