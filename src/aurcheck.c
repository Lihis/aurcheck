/*
 * Copyright (C) 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "aurcheck.h"
#include "aur.h"
#include "pacman.h"
#include "makepkg.h"
#include "package.h"
#include "extract.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

static void print_updates(const struct Packages *packages)
{
	char *message = NULL;
	size_t msg_len = 0;

	for (uint32_t i = 0; i < packages->count; ++i) {
		if (packages->arr[i].update) {
			char *name = packages->arr[i].name;
			size_t name_len = strlen(name);
			char *version = packages->arr[i].version.aur;
			size_t version_len = strlen(version);
			size_t len = (name_len + version_len);

			if (!message) {
				message = malloc(len + 2);
			} else {
				message = realloc(message, (msg_len + len) + 3);
				memcpy(message + msg_len, " ", 1);
				msg_len += 1;
			}

			memcpy(message + msg_len, name, name_len);
			msg_len += name_len;

			memcpy(message + msg_len, "-", 1);
			msg_len += 1;

			memcpy(message + msg_len, version, version_len);
			msg_len += version_len;

			message[msg_len] = '\0';
		}
	}

	printf(" Packages (%d) %s\n", packages->count_upgradable, message);
	free(message);
}

static void print_notfound(const struct Packages *packages)
{
	char *message = NULL;
	size_t msg_len = 0;

	for (uint32_t i = 0; i < packages->count; i++) {
		if (packages->arr[i].not_found) {
			size_t len = strlen(packages->arr[i].name);

			if (!message) {
				message = malloc(len + 1);
			} else {
				message = realloc(message, (msg_len + len) + 2);
				memcpy(message + msg_len, " ", 1);
				msg_len += 1;
			}
			memcpy(message + msg_len, packages->arr[i].name, len);
			msg_len += len;
			message[msg_len] = '\0';
		}
	}

	if (message) {
		fprintf(stderr,
			"\033[93m=> WARN:\033[0m Packages not found in AUR: %s\n",
			message);
		free(message);
	}
}

int conf_prompt()
{
	char prompt[3];

	fgets(prompt, 2, stdin);

	switch (prompt[0]) {
	case 'y':
	case 'Y':
	case '\n':
		break;
	default:
		return 0;
	}

	return 1;
}

int install_package(char *name, int assume_yes, int keep_source,
		    int compile_only, int as_deps, int skip_pgp_check)
{
	struct Packages packages;
	packages_init(&packages);

	packages_add(&packages, name, "0");

	int aur_error = aur_get_info(&packages);
	if (aur_error != AUR_OK) {
		fprintf(stderr,
			"ERROR: Failed to fetch package information from AUR: %s\n",
			aur_get_error(aur_error));
		return 1;
	}

	struct Package *package = packages_find(&packages, name);

	// Download package file
	int ret = download_package(name, package->url);
	if (ret)
		return ret;

	pid_t pid;
	int fd_stdout;
	int fd_stderr;
	int installed = 0;

	pid = fork();
	if (pid == -1) {
		fprintf(stderr, "Fork error with pacman.\n");
		exit(1);
	} else if (pid == 0) {
		if ((fd_stdout = open("/dev/null", O_WRONLY)) == -1) {
			fprintf(stderr, "Could not open \"/dev/null\".");
			exit(1);
		}
		if ((fd_stderr = open("/dev/null", O_WRONLY)) == -1) {
			fprintf(stderr, "Could not open \"/dev/null\".");
			exit(1);
		}

		dup2(fd_stdout, STDOUT_FILENO);
		dup2(fd_stderr, STDERR_FILENO);
		close(fd_stdout);
		close(fd_stderr);
		execl("/usr/bin/pacman", "pacman", "-Q", name, (char *)NULL);
		_exit(1);
	} else {
		int status;
		waitpid(pid, &status, 0);
		if (WIFEXITED(status)) {
			// 1 = package is not installed
			if (WEXITSTATUS(status)) {
				if (!assume_yes) {
					printf("\n Install package \"%s\"? [Y/n] ",
					       name);
					if (!conf_prompt())
						return 0;
				}
			} else {
				// 0 = package is installed
				if (!assume_yes) {
					installed = 1;
					printf("\n Upgrade package \"%s\"? [Y/n] ",
					       name);
					if (!conf_prompt())
						return 0;
				}
			}
		} else {
			fprintf(stderr, "aurcheck: pacman error: \"%d\"\n",
				WEXITSTATUS(status));
		}
	}

	// Extract package tarball
	if (extract_package(CACHE_DIR, name)) {
		fprintf(stderr,
			"ERROR! Failed to extract package \"%s\" tarball, skipping package upgrade.\n",
			name);
		return 1;
	}

	// Invoke makepkg
	if (makepkg(CACHE_DIR, package->base, skip_pgp_check)) {
		fprintf(stderr,
			"ERROR! Compiling package \"%s\" failed, package not upgraded. Source directory was not removed.\n",
			name);
		return 1;
	}

	if (compile_only) {
		if (installed)
			printf("Package compiled, skipping upgrade..\n");
		else
			printf("Package compiled, skipping installation..\n");

		return 0;
	}

	// Invoke install trough pacman
	if (!pacman_install(CACHE_DIR, package->base, as_deps)) {
		if (installed)
			printf(" \nPackage \"%s\" upgraded.\n", name);
		else
			printf(" \nPackage \"%s\" installed.\n", name);
	} else {
		if (installed)
			fprintf(stderr,
				"ERROR! Upgrade of package \"%s\" failed. Source directory was not removed.\n",
				name);
		else
			fprintf(stderr,
				"ERROR! Installation of package \"%s\" failed. Source directory was not removed\n",
				name);
		return 1;
	}

	// remove package source directory
	if (!keep_source)
		if (packages_remove_source(package->base))
			fprintf(stderr,
				"WARNING: Failed to remove package \"%s\" source directory.\n",
				name);

	packages_free(&packages);

	return 0;
}

int upgrade_packages(int upgrade_flag, int assume_yes, int keep_source,
		     int compile_only, int as_deps, int skip_pgp_check)
{
	struct Packages packages;
	enum aur_error aur_err;

	packages_init(&packages);

	// Get pacman's packages which are from AUR
	printf("\033[34m=>\033[0m Querying AUR packages from pacman...\n");
	switch (pacman_get_aur_packages(&packages)) {
	case -1:
		fprintf(stderr, "Failed to get AUR packages from pacman!\n");
		break;
	case 0:
		printf("Your system does not have packages from AUR. Nothing to do.\n");
		return 0;
	default:
		break;
	}

	// AUR query
	printf("\033[34m=>\033[0m Downloading packages information from AUR...\n");
	aur_err = aur_get_info(&packages);
	if (aur_err != AUR_OK) {
		fprintf(stderr, "Error while querying AUR: %s\n",
			aur_get_error(aur_err));
		return -1;
	}

	packages_check_updates(&packages);
	print_notfound(&packages);

	if (packages.count_upgradable) {
		print_updates(&packages);
	} else {
		printf("There is nothing to do\n");
		packages_free(&packages);
		return 0;
	}

	/* Upgrade packages */
	if (!upgrade_flag)
		return 0;

	if (!assume_yes) {
		printf("\n Upgrade packages? [Y/n] ");
		if (!conf_prompt())
			return 0;
	}

	/* Upgrade process */
	printf("\033[34m=>\033[0m Starting upgrade...\n");

	for (int i = 0; i < packages.count; ++i) {
		if (!packages.arr[i].update)
			continue;

		if (packages.arr[i].not_found)
			continue;

		printf("\033[34m=>\033[0m Retrieving package '%s'...\n",
		       packages.arr[i].base);
		if (!packages.arr[i].base) {
			fprintf(stderr,
				"Can't download package %s due package base is missing.\n",
				packages.arr[i].name);
			continue;
		}

		if (download_package(packages.arr[i].base,
				     packages.arr[i].url) != 0) {
			printf(" Download failed.\n");
			continue;
		}

		printf("\033[34m=>\033[0m Building package '%s'...\n",
		       packages.arr[i].name);
		if (extract_package(CACHE_DIR, packages.arr[i].base)) {
			fprintf(stderr,
				"ERROR! Failed to extract package \"%s\" tarball, skipping package upgrade.\n",
				packages.arr[i].name);
			continue;
		}

		// Invoke makepkg
		if (makepkg(CACHE_DIR, packages.arr[i].base, skip_pgp_check)) {
			fprintf(stderr,
				"ERROR! Compiling package '%s' failed, package not upgraded. Source directory was not removed.\n",
				packages.arr[i].name);
			continue;
		}

		if (compile_only) {
			printf("Compile only enabled, skipping upgrade..\n");
			continue;
		}

		// Invoke install trough pacman
		if (pacman_install(CACHE_DIR, packages.arr[i].base, as_deps) == 0) {
			printf("\033[34m=>\033[0m Package \"%s\" upgraded.\n",
			       packages.arr[i].name);

			packages_mark_base_updated(&packages,
						   packages.arr[i].base);

			// remove package source directory
			if (!keep_source)
				if (packages_remove_source(
					    packages.arr[i].base))
					fprintf(stderr,
						"\nWARNING: Failed to remove package \"%s\" source directory.\n",
						packages.arr[i].name);
		} else {
			fprintf(stderr,
				"\nERROR! Upgrade of package \"%s\" failed. Source directory was not removed.\n",
				packages.arr[i].name);
			packages_mark_base_failed(&packages,
						  packages.arr[i].base);
		}
	}

	if (packages.count_failed) {
		fprintf(stderr,
			"\nWARNING: Following packages were not upgraded (%d): ",
			packages.count_failed);
		for (uint32_t i = 0; i < packages.count; ++i)
			if (packages.arr[i].failed)
				fprintf(stderr, "%s ", packages.arr[i].name);
		fprintf(stderr, "\n");
	}

	packages_free(&packages);

	return (packages.count_failed > 0) ? 1 : 0;
}
