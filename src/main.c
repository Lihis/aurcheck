/*
 * Copyright (C) 2015-2017, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "aurcheck.h"
#include "version.h"
#include <stdio.h>
#include <string.h>

void print_version()
{
	printf("aurcheck version v%d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR,
	       VERSION_PATCH);
}

void print_help()
{
	printf("usage: aurcheck [OPTIONS] [...]\n\n");
	printf("  -S                    Display available updates and exit.\n");
	printf("  -u                    Use with \"-S\" to download and install updates.\n");
	printf("      --keep-source     Do not delete content extracted from package tarball.\n");
	printf("      --compile-only    Download and build package but do not install.\n");
	printf("      --yes             Yes to all questions.\n");
	printf("      --version	       Print version and exit.\n");
	printf("  -H, --help            Display this help and exit.\n");
	printf("\n");
	printf("These can be passed to makepkg:\n");
	printf("      --asdeps              Install packages as non-explicitly installed.\n");
	printf("      --skippgpcheck        Do not verify source files with PGP signatures.\n");
}

int main(int argc, char *argv[])
{
	/* Arguments */
	int sync_flag = 0;
	int install_flag = 0;
	int upgrade_flag = 0;
	int keep_source = 0;
	int compile_only = 0;
	int as_deps = 0;
	int skip_pgp_check = 0;
	int assume_yes = 0;
	char *package = NULL;

	/* Check we got arguments */
	if (!argv[1]) {
		fprintf(stderr, "error: no operation specified.\n");
		return 1;
	} else if (strcmp(argv[1], "--help") == 0 ||
		   strcmp(argv[1], "-H") == 0) {
		print_help();
		return 0;
	} else if (strcmp(argv[1], "--version") == 0) {
		print_version();
		return 0;
	}

	for (int i = 1; i < argc; i++) {
		if ((argv[i][0] == '-') && (argv[i][1] != '-')) {
			for (int j = 1; j < strlen(argv[i]); j++) {
				switch (argv[i][j]) {
				case 'S':
					if (argv[i + 1] != NULL &&
					    argv[i + 1][0] != '-') {
						install_flag = 1;
						package = argv[i + 1];
						i++;
						j = (int)strlen(argv[i]);
					} else {
						sync_flag = 1;
					}
					break;
				case 'u':
					upgrade_flag = 1;
					break;
				default:
					fprintf(stderr,
						"aurcheck: Invalid option -- \"%c\"\n",
						argv[i][j]);
					return -1;
				}
			}
		} else if (strcmp(argv[i], "--yes") == 0) {
			assume_yes = 1;
		} else if (strcmp(argv[i], "--keep-source") == 0) {
			keep_source = 1;
		} else if (strcmp(argv[i], "--compile-only") == 0) {
			compile_only = 1;
		} else if (strcmp(argv[i], "--asdeps") == 0) {
			as_deps = 1;
		} else if (strcmp(argv[i], "--skippgpcheck") == 0) {
			skip_pgp_check = 1;
		} else {
			fprintf(stderr, "aurcheck: Invalid option -- \"%s\"\n",
				argv[i]);
			return -1;
		}
	}

	// Install package
	if (install_flag)
		return install_package(package, assume_yes, keep_source,
				       compile_only, as_deps, skip_pgp_check);
	// Display available updates and upgrade
	else if (sync_flag)
		return upgrade_packages(upgrade_flag, assume_yes, keep_source,
					compile_only, as_deps, skip_pgp_check);

	return 0;
}
