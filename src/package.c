/*
 * Copyright (C) 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 500
#include "package.h"
#include "aurcheck.h"
#include <ftw.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <alpm.h>

/* remove directory recursively */
static int rm_source(const char *path, const struct stat *sb, int flag,
		     struct FTW *buf)
{
	int ret;
	(void)sb;
	(void)buf;

	switch (flag) {
	case FTW_D:
	case FTW_DNR:
	case FTW_DP:
		ret = rmdir(path);
		break;
	default:
		ret = unlink(path);
		break;
	}

	return ret;
}

void package_init(struct Package *package)
{
	package->base = NULL;
	package->name = NULL;
	package->version.local = NULL;
	package->version.aur = NULL;
	package->depends = NULL;
	package->url = NULL;
	package->update = 0;
	package->failed = 0;
	package->not_found = 1;
}

void package_free(struct Package *package)
{
	free(package->base);
	free(package->name);
	free(package->version.local);
	free(package->version.aur);
	free(package->depends);
	free(package->url);
}

void packages_init(struct Packages *packages)
{
	packages->count = 0;
	packages->count_upgradable = 0;
	packages->count_failed = 0;
	packages->arr = NULL;
}

void packages_init_sub(struct Packages *packages)
{
	packages->count++;

	packages->arr = realloc(packages->arr,
				packages->count * sizeof(*packages->arr));
	if (!packages->arr) {
		fprintf(stderr, "Failed to allocate memory!\n");
		exit(1);
	}

	struct Package package;
	package_init(&package);
	memcpy(&packages->arr[packages->count - 1], &package,
	       sizeof(struct Package));
}

void packages_add(struct Packages *packages, const char *name,
		  const char *version)
{
	packages_init_sub(packages);

	struct Package *package = &packages->arr[packages->count - 1];

	package->name = calloc(strlen(name) + 1, sizeof(char));
	memcpy(package->name, name, strlen(name));

	package->version.local = calloc(strlen(version) + 1, sizeof(char));
	memcpy(package->version.local, version, strlen(version));
}

void packages_free(struct Packages *packages)
{
	if (packages->count < 1)
		return;

	for (uint32_t i = 0; i < packages->count; ++i) {
		package_free(&packages->arr[i]);
	}
	free(packages->arr);
}

void packages_check_updates(struct Packages *packages)
{
	for (uint32_t i = 0; i < packages->count; i++) {
		struct Package *package;
		int ret;

		package = &packages->arr[i];

		if (package->not_found) {
			continue;
		}

		ret = package_version_cmp(package);
		if (ret == -1) {
			fprintf(stderr,
				"\033[93m=> WARN:\033[0m %s local version (%s) newer than AUR version (%s).\n",
				package->name, package->version.local,
				package->version.aur);
		} else if (ret == 1) {
			package->update = 1;
			packages->count_upgradable++;
		}
	}
}

void packages_mark_base_updated(struct Packages *packages, const char *base)
{
	for (uint32_t i = 0; i < packages->count; ++i) {
		struct Package *package = &packages->arr[i];

		if (package->not_found) {
			continue;
		}

		if (strcmp(base, package->base) == 0) {
			package->update = 0;
		}
	}
}

void packages_mark_base_failed(struct Packages *packages, const char *base)
{
	for (uint32_t i = 0; i < packages->count; ++i) {
		struct Package *package = &packages->arr[i];

		if (package->not_found) {
			continue;
		}

		if (strcmp(base, package->base) == 0) {
			package->failed = 1;
			packages->count_failed++;
		}
	}
}

int package_version_cmp(const struct Package *package)
{
	return alpm_pkg_vercmp(package->version.aur, package->version.local);
}

struct Package *packages_find(const struct Packages *package, const char *name)
{
	for (int i = 0; i < package->count; i++) {
		if (strcmp(package->arr[i].name, name) == 0) {
			return &package->arr[i];
		}
	}

	return NULL;
}

int packages_remove_source(const char *name)
{
	int ret;
	int size;
	char *package_path;

	size = snprintf(NULL, 0, "%s/%s", CACHE_DIR, name);
	package_path = malloc((size_t)size + 1);
	if (!package_path) {
		fprintf(stderr, "Memory error.");
		exit(1);
	}
	snprintf(package_path, (size_t)size + 1, "%s/%s", CACHE_DIR, name);

	ret = nftw(package_path, rm_source, 20, FTW_PHYS | FTW_DEPTH);

	return ret;
}
