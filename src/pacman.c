/*
 * Copyright (C) 2016-2017, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "pacman.h"
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <fnmatch.h>
#include <memory.h>
#include <stdlib.h>
#include <wait.h>

int pacman_get_aur_packages(struct Packages *packages)
{
	const uint8_t BUF_SIZE = 64;
	size_t offset = 0;
	FILE *fp;
	char tmp[BUF_SIZE];
	char *buffer;
	size_t buf_len = BUF_SIZE;
	size_t line_start = 0;

	fp = popen("/usr/bin/pacman -Qm", "r");
	if (!fp) {
		return -1;
	}

	buffer = malloc(BUF_SIZE);
	if (!buffer) {
		fprintf(stderr, "Memory error.\n");
		exit(1);
	}

	while (fgets(tmp, sizeof(tmp), fp) != NULL) {
		int read = strlen(tmp);

		if ((offset + read) > buf_len) {
			buf_len += BUF_SIZE;

			if ((offset + read) > buf_len) {
				buf_len += read;
			}

			buffer = realloc(buffer, buf_len);
			if (!buffer) {
				fprintf(stderr, "Memory error.\n");
				exit(1);
			}
		}

		memmove(buffer + offset, tmp, read);
		offset += read;
	}

	for (size_t i = 0; i < buf_len; i++) {
		if (buffer[i] == '\n') {
			char *name = NULL;
			char *version = NULL;
			size_t space_pos = 0;

			for (size_t j = line_start; j <= i; j++) {
				if (name && j == i) {
					size_t len = (j - space_pos);
					version = malloc(len + 1);
					memcpy(version, buffer + space_pos,
					       len);
					version[len] = '\0';
				}

				if (buffer[j] == ' ') {
					size_t len = (j - line_start);
					name = malloc(len + 1);
					memcpy(name, buffer + line_start, len);
					name[len] = '\0';
					space_pos = (j + 1);
				}
			}

			if (name && version) {
				packages_add(packages, name, version);
				free(name);
				free(version);
			} else {
				if (name)
					free(name);

				if (version)
					free(version);
			}

			line_start = (i + 1);
		}
	}

	free(buffer);

	if (pclose(fp))
		return -1;

	return packages->count;
}

char *pacman_get_built_packages()
{
	DIR *dir;
	struct dirent *ent;
	int size;
	char *pattern = "*.pkg.tar*";
	char *packages = NULL;

	if (!(dir = opendir("./"))) {
		fprintf(stderr,
			"ERROR: Could not open package source directory.\n");
		return NULL;
	}

	while ((ent = readdir(dir)) != NULL) {
		if ((fnmatch(pattern, ent->d_name, 0)) == 0) {
			char *tmp;

			if (!packages) {
				size = snprintf(NULL, 0, "%s", ent->d_name);
				tmp = malloc((size_t)size + 1);
				if (!tmp) {
					fprintf(stderr, "Memory error.\n");
					exit(1);
				}
				snprintf(tmp, (size_t)size + 1, "%s",
					 ent->d_name);
			} else {
				size = snprintf(NULL, 0, "%s %s", packages,
						ent->d_name);
				tmp = malloc((size_t)size + 1);
				if (!tmp) {
					fprintf(stderr, "Memory error.\n");
					exit(1);
				}
				snprintf(tmp, (size + 1), "%s %s", packages,
					 ent->d_name);
			}

			packages = realloc(packages, size + 1);
			if (!packages) {
				fprintf(stderr, "Memory error.\n");
				exit(1);
			}
			snprintf(packages, (size + 1), "%s", tmp);

			free(tmp);
		}
	}
	closedir(dir);

	return packages;
}

int pacman_install(char *cache_path, char *pkg_base, int as_deps)
{
	char *package_files;
	int size;
	char *pacman;
	pid_t pid;
	int status;

	chdir(cache_path);
	chdir(pkg_base);

	package_files = pacman_get_built_packages();
	if (!package_files) {
		fprintf(stderr, "Package file(s) not found!\n");
		return -1;
	}

	if (as_deps == 1) {
		size = snprintf(NULL, 0, "pacman -U --asdeps %s", package_files);
	} else {
		size = snprintf(NULL, 0, "pacman -U %s", package_files);
	}
	pacman = malloc(size + 1);
	if (!pacman) {
		fprintf(stderr, "Memory error.\n");
		exit(1);
	}
	if (as_deps == 1) {
		snprintf(pacman, (size + 1), "pacman -U --asdeps %s", package_files);
	} else {
		snprintf(pacman, (size + 1), "pacman -U %s", package_files);
	}

	pid = fork();
	if (pid == 0) {
		execl("/usr/bin/sudo", "sudo", "sh", "-c", pacman,
		      (char *)NULL);
		free(pacman);
		free(package_files);
		return 1;
	} else {
		waitpid(pid, &status, 0);
	}
	free(pacman);
	free(package_files);

	return status;
}
