/*
 * Copyright (C) 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AURCHECK_AUR_H
#define AURCHECK_AUR_H

#include "package.h"

enum aur_error {
	AUR_OK = 0,

	AUR_HTTP_ERROR = 1,
	AUR_RESPONSE_ERROR = 2,

	AUR_MISSING_KEY_TYPE = 3,
	AUR_MISSING_KEY_RESULTCOUNT = 4,
	AUR_MISSING_KEY_RESULTS = 5,
	AUR_PARSING_COUNT_MISMATCH = 6,

	AUR_PACKAGE_NOT_FOUND = 7,
};

/**
 * @brief Get error description
 *
 * @param error
 * @return const char *
 */
const char *aur_get_error(enum aur_error error);

/**
 * @brief Fetch packages information from AUR to memory
 *
 * Fetch information for all packages stored in @p packages
 *
 * @param packages
 * @return aur_error_t
 */
enum aur_error aur_get_info(struct Packages *packages);

/**
 * @brief Download package
 *
 * Downloads package from AUR pointed by @p uri and saves it into
 * cache directory with name of @p name.
 *
 * @param name
 * @param path
 * @return int
 */
int download_package(char *name, char *uri);

#endif //AURCHECK_AUR_H
