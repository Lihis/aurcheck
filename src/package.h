/*
 * Copyright (C) 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AURCHECK_PACKAGE_H
#define AURCHECK_PACKAGE_H

#include <stdint.h>

struct Version {
	char *local;
	char *aur;
};

struct Package {
	char *base;
	char *name;
	struct Version version;
	void *depends;
	char *url;
	uint8_t update;
	uint8_t failed;
	uint8_t not_found;
};

struct Packages {
	uint32_t count;
	uint32_t count_upgradable;
	uint32_t count_failed;
	struct Package *arr;
};

/**
 * @brief Initialize @p packages
 *
 * @param packages
 */
void packages_init(struct Packages *packages);

/**
 * @brief Add package with name @p name and version @p version to @p packages
 *
 * @param packages
 * @param name
 * @param version
 */
void packages_add(struct Packages *packages, const char *name,
		  const char *version);

/**
 * @brief Free @p packages
 *
 * @param packages
 */
void packages_free(struct Packages *packages);

/**
 * @brief Check for upgradable packages in @p packages
 *
 * @param packages
 */
void packages_check_updates(struct Packages *packages);

/**
 * @brief Mark all packages in @p base package as updated
 *
 * @param packages
 * @param base
 */
void packages_mark_base_updated(struct Packages *packages, const char *base);

/**
 * @brief Mark package base @p base failed
 *
 * @param packages
 * @param base
 */
void packages_mark_base_failed(struct Packages *packages, const char *base);

/**
 * @brief Compare @p package local version against AUR version
 *
 * Returns values:
 *  -1 = local version is newer
 *   0 = up-to-date
 *   1 = local version is older
 *
 * @param package
 * @return int
 */
int package_version_cmp(const struct Package *package);

/**
 * @brief Find package by @p name from @p packages
 *
 * @param package
 * @param name
 * @return struct Package *
 */
struct Package *packages_find(const struct Packages *package, const char *name);

/**
 * @brief Remove package build directory
 *
 * @param path
 * @return int
 */
int packages_remove_source(const char *path);

#endif //AURCHECK_PACKAGE_H
