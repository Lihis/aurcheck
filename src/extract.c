/*
 * Copyright (C) 2016, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "extract.h"
#include <archive.h>
#include <archive_entry.h>
#include <stdlib.h>

int copy_data(struct archive *ar, struct archive *aw)
{
	int r;
	const void *buff;
	size_t size;
	off_t offset;

	for (;;) {
		r = archive_read_data_block(ar, &buff, &size, &offset);
		if (r == ARCHIVE_EOF)
			return (ARCHIVE_OK);
		if (r < ARCHIVE_OK)
			return r;

		r = archive_write_data_block(aw, buff, size, offset);
		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(aw));
			return r;
		}
	}
}

int extract_package(char *cache_path, char *filename)
{
	struct archive *archive;
	struct archive *ext;
	struct archive_entry *entry;
	int flags;
	int r;
	int size;
	char *file_path;

	chdir(cache_path);

	size = snprintf(NULL, 0, "%s/%s.tar.gz", cache_path, filename);
	file_path = malloc(size + 1);
	if (!file_path) {
		fprintf(stderr, "Memory error.");
		exit(1);
	}
	snprintf(file_path, size + 1, "%s/%s.tar.gz", cache_path, filename);

	/* Extracting flags */
	flags = ARCHIVE_EXTRACT_TIME;
	flags |= ARCHIVE_EXTRACT_PERM;
	flags |= ARCHIVE_EXTRACT_FFLAGS;

	archive = archive_read_new();
	archive_read_support_format_tar(archive);
	archive_read_support_filter_gzip(archive);
	ext = archive_write_disk_new();
	archive_write_disk_set_options(ext, flags);
	archive_write_disk_set_standard_lookup(ext);
	if (archive_read_open_filename(archive, file_path, 10240) != 0)
		return 1;

	for (;;) {
		r = archive_read_next_header(archive, &entry);
		if (r == ARCHIVE_EOF)
			break;
		if (r < ARCHIVE_OK)
			fprintf(stderr, "%s\n", archive_error_string(archive));
		if (r < ARCHIVE_WARN)
			return 1;

		r = archive_write_header(ext, entry);
		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(ext));
		} else if (archive_entry_size(entry) > 0) {
			r = copy_data(archive, ext);
			if (r < ARCHIVE_OK)
				fprintf(stderr, "%s\n",
					archive_error_string(ext));
			if (r < ARCHIVE_WARN)
				return 1;
		}

		r = archive_write_finish_entry(ext);
		if (r < ARCHIVE_OK)
			fprintf(stderr, "%s\n", archive_error_string(ext));
		if (r < ARCHIVE_WARN)
			return 1;
	}

	archive_read_close(archive);
	archive_read_free(archive);
	archive_write_close(ext);
	archive_write_free(ext);

	free(file_path);

	return 0;
}
