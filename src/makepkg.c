/*
 * Copyright (C) 2016, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "makepkg.h"
#include <unistd.h>
#include <malloc.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

void makepkg_add_arg(char ***args, int *args_len, const char *arg)
{
	size_t len;

	*args = realloc(*args, (*args_len + 2) * sizeof(*args));
	if (*args == NULL) {
		fprintf(stderr, "Memory error.\n");
		exit(1);
	}

	len = strlen(arg);
	(*args)[*args_len] = malloc(len + 1);
	if ((*args)[*args_len] == NULL) {
		fprintf(stderr, "Memory error.\n");
		exit(1);
	}

	snprintf((*args)[*args_len], (len + 1), "%s", arg);
	(*args_len)++;
	(*args)[*args_len] = NULL;
}

int makepkg(char *cache_path, char *pkg_base, int skip_pgp_check)
{
	int ret;
	int size;
	char *package_path;
	int arg_len;
	char **args;

	arg_len = 0;
	args = NULL;

	makepkg_add_arg(&args, &arg_len, "makepkg");
	makepkg_add_arg(&args, &arg_len, "-scr");
	if (skip_pgp_check == 1)
		makepkg_add_arg(&args, &arg_len, "--skippgpcheck");

	size = snprintf(NULL, 0, "%s/%s", cache_path, pkg_base);
	package_path = malloc(size + 1);
	if (!package_path) {
		fprintf(stderr, "Memory error.\n");
		exit(1);
	}
	snprintf(package_path, size + 1, "%s/%s", cache_path, pkg_base);

	chdir(package_path);
	free(package_path);

	if (fork() == 0) {
		execv("/usr/bin/makepkg", args);
		ret = 1;
	} else {
		ret = 0;
		wait(NULL);
	}

	for (int i = 0; i <= arg_len; i++)
		free(args[i]);
	free(args);

	return ret;
}
