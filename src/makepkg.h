/*
 * Copyright (C) 2016, 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This file is part of aurcheck.
 *
 * aurcheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aurcheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aurcheck.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AURCHECK_MAKEPKG_H
#define AURCHECK_MAKEPKG_H

/**
 * @brief Invoke makepkg
 *
 * @param cache_path
 * @param pkg_base
 * @param skip_pgp_check - Skip PGP check
 * @return int
 */
int makepkg(char *cache_path, char *pkg_base, int skip_pgp_check);

#endif //AURCHECK_MAKEPKG_H
