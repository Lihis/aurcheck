# aurcheck

Another AUR helper. Written in C. Under occasional development and currently
provides checking for updates via AUR RPC interface, installing and upgrading
AUR packages. Does not check package dependencies on install/upgrade.

## Commands

To get a complete list of the script commands you can run `aurcheck --help`
or `aurcheck -H`.
