#!/usr/bin/env python3
import argh
from lxml import etree


def _test_case(parent, element):
    ret = etree.SubElement(parent, 'testcase')
    ret.set('name', '')
    ret.set('classname', '')

    message = ''
    for elem in element:
        tag = etree.QName(elem).localname
        if tag == 'description':
            ret.set('classname', elem.text)
        elif tag == 'id':
            ret.set('name', elem.text)
        elif tag == 'message':
            message = elem.text
        elif tag == 'duration':
            duration = float(elem.text)
            if duration < 0.0:
                duration = 0.0
            ret.set('time', '%f' % duration)

    result = element.get('result')
    if result != 'success':
        status = etree.SubElement(ret, result)
        status.text = message

    return ret


def _test_suite(parent, element):
    ret = etree.SubElement(parent, 'testsuite')
    ret.set('tests', '0')
    ret.set('skipped', '0')
    ret.set('failures', '0')
    ret.set('errors', '0')
    ret.set('time', '0')

    for suite in element:
        tag = etree.QName(suite).localname
        if tag == 'title':
            ret.set('name', suite.text)
        elif tag == 'test':
            elem = _test_case(ret, suite)

            exec_time = float(elem.get('time'))
            if exec_time < 0.0:
                exec_time = 0

            ret.set('time', str(float(ret.get('time')) + exec_time))
            ret.append(elem)
            ret.set('tests', str((int(ret.get('tests')) + 1)))
            if elem.find('skipped') is not None:
                ret.set('skipped', str(int(ret.get('skipped')) + 1))
            elif elem.find('errors') is not None:
                ret.set('errors', str(int(ret.get('errors')) + 1))
            elif elem.find('failure') is not None:
                ret.set('failures', str(int(ret.get('failures')) + 1))

    return ret


def convert_check2junit(check_xml, output_xml):
    tree = etree.parse(check_xml)

    testsuites = etree.Element('testsuites')
    junit = etree.ElementTree(testsuites)
    suite_id = 0

    for child in tree.getroot():
        tag = etree.QName(child).localname
        if tag == 'suite':
            elem = _test_suite(testsuites, child)
            testsuites.append(elem)
            elem.set('id', str(suite_id))
            suite_id += 1

    junit.write(output_xml, pretty_print=True, xml_declaration=True, encoding='utf-8')


if __name__ == '__main__':
    argh.dispatch_command(convert_check2junit)
