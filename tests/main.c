#include "test_aur.h"

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;
	int num_failed;
	SRunner *sr;

	sr = srunner_create(make_aur_suite());

	srunner_set_xml(sr, "test_results.xml");

	srunner_run_all(sr, CK_VERBOSE);
	num_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return num_failed;
}