find_package(PkgConfig REQUIRED)
pkg_check_modules(CHECK REQUIRED check)

add_executable(tests
               main.c
               test_aur.c
               test_aur.h
               )

target_include_directories(tests
                           PRIVATE
                           ${CHECK_INCLUDE_DIRS}
                           )

target_link_libraries(tests
                      PRIVATE
                      aur
                      ${CHECK_LIBRARIES}
                      )

target_compile_options(tests
                       PRIVATE
                       ${CHECK_CFLAGS_OTHER}
                       )

set_target_properties(tests PROPERTIES COMPILE_FLAGS "-Wall -Wextra")
