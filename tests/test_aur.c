#include "test_aur.h"
#include <aur.h>

struct Packages packages;

extern int parse_json(struct Packages *packages, const char *json);

void setup()
{
	packages_init(&packages);
	packages_add(&packages, "awesome-package", "0.1.0-1");
}

void teardown()
{
	packages_free(&packages);
}

START_TEST(ReturnsMissingKeyType_WhenTypeKeyIsMissing)
{
	ck_assert_int_eq(parse_json(&packages, "{\"version\": 5 }"),
			 AUR_MISSING_KEY_TYPE);
}
END_TEST

START_TEST(ReturnsResponseError_WhenTypeIsError)
{
	ck_assert_int_eq(parse_json(&packages, "{\"type\":\"error\"}"),
			 AUR_RESPONSE_ERROR);
}
END_TEST

START_TEST(ReturnsMissingResultcount_WhenResultcountKeyIsMissing)
{
	ck_assert_int_eq(parse_json(&packages, "{\"type\":\"multiinfo\"}"),
			 AUR_MISSING_KEY_RESULTCOUNT);
}
END_TEST

START_TEST(ReturnsPackageNotFound_WhenResultcountIsZero)
{
	ck_assert_int_eq(
		parse_json(&packages,
			   "{\"type\":\"multiinfo\", \"resultcount\": 0}"),
		AUR_PACKAGE_NOT_FOUND);
}
END_TEST

START_TEST(ReturnsMissingResults_WhenResultsKeyIsMissing)
{
	ck_assert_int_eq(
		parse_json(&packages,
			   "{\"type\":\"multiinfo\", \"resultcount\": 1}"),
		AUR_MISSING_KEY_RESULTS);
}
END_TEST

START_TEST(ReturnsParsingCountMismatch_WhenResultCountDoesNotMatchParsedCount)
{
	const char *json = "{\n"
			   "  \"version\": 5,\n"
			   "  \"type\": \"multiinfo\",\n"
			   "  \"resultcount\": 2,\n"
			   "  \"results\": []\n"
			   "}\n";

	ck_assert_int_eq(parse_json(&packages, json),
			 AUR_PARSING_COUNT_MISMATCH);
}
END_TEST

START_TEST(ReturnsOK_WhenJSONIsValid)
{
	const char *json =
		"{\n"
		"  \"version\": 5,\n"
		"  \"type\": \"multiinfo\",\n"
		"  \"resultcount\": 1,\n"
		"  \"results\": [\n"
		"    {\n"
		"      \"ID\": 1,\n"
		"      \"Name\": \"awesome-package\",\n"
		"      \"PackageBaseID\": 1,\n"
		"      \"PackageBase\": \"awesome-package\",\n"
		"      \"Version\": \"0.1.0-1\",\n"
		"      \"Description\": \"An awesome description for a package\",\n"
		"      \"URL\": \"https:\\/\\/example.com\\/\",\n"
		"      \"NumVotes\": 1,\n"
		"      \"Popularity\": 0.1,\n"
		"      \"OutOfDate\": null,\n"
		"      \"Maintainer\": \"nobody\",\n"
		"      \"FirstSubmitted\": 1353157042,\n"
		"      \"LastModified\": 1487592049,\n"
		"      \"URLPath\": \"\\/cgit\\/aur.git\\/snapshot\\/awesome-package.tar.gz\",\n"
		"      \"License\": [ \"custom\" ],\n"
		"      \"Keywords\": [ \"fancy\", \"keyword\" ]\n"
		"    }\n"
		"  ]\n"
		"}";

	ck_assert_int_eq(parse_json(&packages, json), AUR_OK);
	ck_assert_str_eq(packages.arr[0].version.aur, "0.1.0-1");
}
END_TEST

START_TEST(ReturnsOK_WhenJSONIsValidAndHasManyPackages)
{
	const char *json =
		"{\n"
		"  \"version\": 5,\n"
		"  \"type\": \"multiinfo\",\n"
		"  \"resultcount\": 3,\n"
		"  \"results\": [\n"
		"    {\n"
		"      \"ID\": 1,\n"
		"      \"Name\": \"awesome-package\",\n"
		"      \"Version\": \"0.1.0-1\",\n"
		"      \"URLPath\": \"\\/cgit\\/aur.git\\/snapshot\\/awesome-package.tar.gz\",\n"
		"    },\n"
		"    {\n"
		"      \"ID\": 2,\n"
		"      \"Name\": \"nice-package\",\n"
		"      \"Version\": \"1.0.0-1\",\n"
		"      \"URLPath\": \"\\/cgit\\/aur.git\\/snapshot\\/nice-package.tar.gz\",\n"
		"    },\n"
		"    {\n"
		"      \"ID\": 3,\n"
		"      \"Name\": \"legit-package\",\n"
		"      \"Version\": \"2.1.0-1\",\n"
		"      \"URLPath\": \"\\/cgit\\/aur.git\\/snapshot\\/legit-package.tar.gz\",\n"
		"    }\n"
		"  ]\n"
		"}";

	packages_add(&packages, "nice-package", "1.0.0-1");
	packages_add(&packages, "legit-package", "2.1.0-1");

	ck_assert_int_eq(parse_json(&packages, json), AUR_OK);
	ck_assert_int_eq(packages.count, 3);
	ck_assert_str_eq(packages.arr[0].version.aur, "0.1.0-1");
	ck_assert_str_eq(packages.arr[1].version.aur, "1.0.0-1");
	ck_assert_str_eq(packages.arr[2].version.aur, "2.1.0-1");
}
END_TEST

Suite *make_aur_suite()
{
	Suite *s;
	TCase *tc_aur;

	s = suite_create("AUR");
	tc_aur = tcase_create("API");

	tcase_add_checked_fixture(tc_aur, setup, teardown);

	tcase_add_test(tc_aur, ReturnsMissingKeyType_WhenTypeKeyIsMissing);
	tcase_add_test(tc_aur, ReturnsResponseError_WhenTypeIsError);
	tcase_add_test(tc_aur,
		       ReturnsMissingResultcount_WhenResultcountKeyIsMissing);
	tcase_add_test(tc_aur, ReturnsPackageNotFound_WhenResultcountIsZero);
	tcase_add_test(tc_aur, ReturnsMissingResults_WhenResultsKeyIsMissing);
	tcase_add_test(
		tc_aur,
		ReturnsParsingCountMismatch_WhenResultCountDoesNotMatchParsedCount);
	tcase_add_test(tc_aur, ReturnsOK_WhenJSONIsValid);
	tcase_add_test(tc_aur, ReturnsOK_WhenJSONIsValidAndHasManyPackages);

	suite_add_tcase(s, tc_aur);

	return s;
}
