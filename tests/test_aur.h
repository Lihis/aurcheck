#ifndef AURCHECK_TEST_AUR_H
#define AURCHECK_TEST_AUR_H

#include <check.h>

Suite *make_aur_suite();

#endif //AURCHECK_TEST_AUR_H
